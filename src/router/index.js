import Vue from 'vue'
import VueRouter from 'vue-router'
import Inventory from '../components/Inventory.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Inventory',
    component: Inventory
  },
  {
    path: '/sales',
    name: 'Sales',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "sales" */ '../components/Sales.vue')
    }
  },
  {
    path: '/purchases',
    name: 'Purchases',
    component: function () {
      return import('../components/Purchases.vue')
    }
  },
  {
    path: '/suppliers',
    name: 'Suppliers',
    component: function () {
      return import('../components/Suppliers.vue')
    }
  },
  {
    path: '/transfers',
    name: 'Transfers',
    component: function () {
      return import('../components/Transfers.vue')
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
