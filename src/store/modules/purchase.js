
const initState = () => {
  return {
    items: [],
    filters: {

    }
  }
}

const state = {
  ...initState()
}


const actions = {
  async loadTable ({ commit }) {

    // set params
    // call and send params to service, service will get the data and send it back here

    let purchases = ''

    // call service here; remove string
    purchases = await 'test purchases'

    // send data to setTable
    commit('setTable', purchases)
  }
}

const mutations = {
  setTable (state, payload) {
    state.items = payload
  }
}

export const purchase = {
  namespaced: true,
  state,
  actions,
  mutations
}